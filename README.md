# README #

GRO-MEX stands for GROMACS MATLAB Exchange. The GRO-MEX repository contains MATLAB tools to enable parsing of GROMACS binary files including .trr, .dat and .fm files.

## What is this repository for? ##

* Share tools to enable file exchange between GROMACS and MATLAB.

## How do I get set up? ##

* Summary of set up:
Clone the biosimpublic repository to your hard drive and include its path into the MATLAB startup.m file by adding a line like this:
*addpath_recurse /path_to_biosim_repository/*

* Dependencies:
Some functions depend on parseargs.m and/or progressbar.m developed by other software engineers. The tools can be downloaded from [http://www.mathworks.com/matlabcentral/fileexchange ](http://www.mathworks.com/matlabcentral/fileexchange)

* How to run tests:
Binary files to test parsing of functions will be provided at: https://drive.google.com/open?id=0B4L2_-k6l60sT2FZZjg4eWx0RGM


## Contribution guidelines ##

* Writing tests

* Code review


## Who do I talk to? ##

* Repo owner or admin
* Other community or team contact

## Licence ##
This work of BioSimVienna is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

![http://creativecommons.org/licenses/by-nc/4.0/](https://i.creativecommons.org/l/by-nc/4.0/88x31.png "CC BY NC")
```
#!

Author:       Reiner J. Ribarics
Affiliation:  Dept. f. Biosimulation and Bioinformatics, Medical University of Vienna
Email:        reiner.ribarics@gmail.com
Keywords:     MATLAB, GROMACS, parser, import, TRR, FM, DAT, PFA, PFR, files
```
